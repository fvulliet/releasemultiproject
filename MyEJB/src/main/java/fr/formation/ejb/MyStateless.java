package fr.formation.ejb;

import javax.ejb.Stateless;


@Stateless
public class MyStateless implements IStateless{

	int cpt=0;
	
	@Override
	public int compteur() {
		
		return cpt++;
	}

}

package fr.formation.ejb;

import javax.ejb.Stateful;


@Stateful
public class MyStateFul implements IStateful{

	int cpt=0;
	
	@Override
	public int compteur() {
		
		return cpt++;
	}

}
